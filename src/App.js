import React from 'react'
import { Route } from 'react-router-dom'
import Home from './Home'
import About from './About'
import './App.css'

const App = () => (
  <div>
    <main>
      <Route path="/" component={Home} />
      <Route exact path="/about-us" component={About} />
    </main>
  </div>
)

export default App
