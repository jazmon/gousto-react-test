import React from 'react'
import styled, { css } from 'styled-components'

const Container = styled.li`
  list-style: none;
  display: flex;

  padding: 1rem;
  &:first-of-type {
    padding-left: 0;
  }
  min-width: fit-content;
  flex-direction: column;
  ${props =>
    props.selected &&
    css`
      font-weight: bold;
      text-decoration: underline;
    `}
`

const CategoryItem = ({ category, onClick, selected }) => (
  <Container selected={selected} onClick={onClick}>
    <span>{category.title}</span>
  </Container>
)

export default CategoryItem
