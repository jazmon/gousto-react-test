import React, { useEffect } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  fetchCategoriesAsync,
  fetchProductsAsync,
  getCategories,
  getProducts,
  initSelectCategory,
  getProductsForSelectedCategory,
  getProductsForSelectedCategorySearch,
  getSelectedCategory,
  initSetProductSearch,
  initSetCategory,
} from './redux/home'
import CategoryItem from './CategoryItem'
import ProductItem from './ProductItem'

const HorizontalList = styled.ul`
  display: flex;
  flex-wrap: nowrap;
  flex-direction: row;
  overflow-x: hidden;
  padding: 0;
`

const List = styled.ul`
  padding: 0;
`

const Container = styled.div`
  padding: 0.5rem 2rem 0.5rem 4rem;
`

const Home = ({
  fetchCategories,
  categories,
  fetchProducts,
  selectedCategory,
  selectCategory,
  setCategory,
  productSearch,
  setProductSearch,
  pathname,
  productsForSelectedCategorySearch,
}) => {
  useEffect(() => {
    fetchCategories()
    fetchProducts()
  }, [])

  useEffect(() => {
    if (pathname.includes('/category/')) {
      if (categories.length === 0) {
        selectCategory(null)
        return
      }
      const title = pathname.split('/')[2]
      const category = categories.find(category => category.title === title)
      if (category) {
        setCategory(category.id)
      }
    } else if (pathname === '/') {
      selectCategory(null)
      return
    }
  }, [pathname])

  return (
    <div>
      <Container>
        <HorizontalList>
          {categories.slice(0, 6).map(category => (
            <CategoryItem
              key={category.id}
              category={category}
              onClick={() => selectCategory(category.id)}
              selected={category.id === selectedCategory}
            />
          ))}
        </HorizontalList>
      </Container>
      <Container>
        <input
          type="text"
          name="productSearch"
          value={productSearch}
          onChange={event => setProductSearch(event.target.value)}
        />
      </Container>
      <Container>
        <List>
          {productsForSelectedCategorySearch.map(product => (
            <ProductItem key={product.id} product={product} />
          ))}
        </List>
      </Container>
    </div>
  )
}

const mapStateToProps = state => ({
  categories: getCategories(state),
  products: getProducts(state),
  selectedCategory: getSelectedCategory(state),
  selectedCategoryProducts: getProductsForSelectedCategory(state),
  productsForSelectedCategorySearch: getProductsForSelectedCategorySearch(
    state
  ),
  productSearch: state.home.productSearch,
  pathname: state.router.location.pathname,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchCategories: () => fetchCategoriesAsync(),
      fetchProducts: () => fetchProductsAsync(),
      selectCategory: categoryId => initSelectCategory(categoryId),
      setCategory: categoryId => initSetCategory(categoryId),
      setProductSearch: productSearch => initSetProductSearch(productSearch),
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
