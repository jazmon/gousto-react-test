import React, { useState } from 'react'
import styled from 'styled-components'

const ListItem = styled.li`
  list-style: none;
`

const Title = styled.p`
  font-weight: ${props => (props.selected ? 'bold' : 'normal')};
`

const Description = styled.p`
  padding-bottom: 1rem;
`

const ProductItem = ({ product }) => {
  const [selected, select] = useState(null)
  return (
    <ListItem onClick={() => select(!selected)}>
      <Title selected={selected}>{product.title}</Title>
      {selected && <Description>{product.description}</Description>}
    </ListItem>
  )
}

export default ProductItem
