// credit: https://github.com/github/fetch#handling-http-error-statuses
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    const error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

export const fetchCategories = async () => {
  const response = await fetch('/products/v2.0/categories')
  checkStatus(response)

  const data = await response.json()
  if (data.status !== 'ok') {
    throw new Error('Status not ok')
  }

  return data.data
}

export const fetchProducts = async () => {
  const response = await fetch(
    '/products/v2.0/products?includes[]=categories&image_sizes[]=365'
  )
  checkStatus(response)

  const data = await response.json()
  if (data.status !== 'ok') {
    throw new Error('Status not ok')
  }

  return data.data
}
