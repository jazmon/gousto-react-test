import { createSelector } from 'reselect'
import { push, replace } from 'connected-react-router'
import { fetchCategories, fetchProducts } from '../api'

// Action types
export const FETCH_CATEGORIES_START = 'home/FETCH_CATEGORIES_START'
export const FETCH_CATEGORIES_SUCCESS = 'home/FETCH_CATEGORIES_SUCCESS'
export const FETCH_CATEGORIES_FAILURE = 'home/FETCH_CATEGORIES_FAILURE'
export const FETCH_PRODUCTS_START = 'home/FETCH_PRODUCTS_START'
export const FETCH_PRODUCTS_SUCCESS = 'home/FETCH_PRODUCTS_SUCCESS'
export const FETCH_PRODUCTS_FAILURE = 'home/FETCH_PRODUCTS_FAILURE'
export const SELECT_CATEGORY = 'home/SELECT_CATEGORY'
export const SET_PRODUCT_SEARCH = 'home/SET_PRODUCT_SEARCH'

// Action creators
export const fetchCategoriesStart = () => ({ type: FETCH_CATEGORIES_START })
export const fetchCategoriesSuccess = categories => ({
  type: FETCH_CATEGORIES_SUCCESS,
  payload: categories,
})
export const fetchCategoriesFailure = error => ({
  type: FETCH_CATEGORIES_FAILURE,
  payload: error,
})

export const fetchCategoriesAsync = () => {
  return async dispatch => {
    dispatch(fetchCategoriesStart())

    try {
      const categories = await fetchCategories()
      dispatch(fetchCategoriesSuccess(categories))
    } catch (error) {
      dispatch(fetchCategoriesFailure(error))
    }
  }
}

export const fetchProductsStart = () => ({ type: FETCH_PRODUCTS_START })
export const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: products,
})
export const fetchProductsFailure = error => ({
  type: FETCH_PRODUCTS_FAILURE,
  payload: error,
})

export const fetchProductsAsync = () => {
  return async dispatch => {
    dispatch(fetchProductsStart())

    try {
      const products = await fetchProducts()
      dispatch(fetchProductsSuccess(products))
    } catch (error) {
      dispatch(fetchProductsFailure(error))
    }
  }
}

export const initSetCategory = category => ({
  type: SELECT_CATEGORY,
  payload: category,
})

export const initSelectCategory = category => {
  return async (dispatch, getState) => {
    const { home } = getState()
    const currentCategory = home.selectedCategory

    if (home.categories.length === 0) {
      dispatch(replace('/'))
      return
    }

    if (category === null) {
      dispatch(initSetCategory(category))
      return
    }
    const categoryName = home.categories.find(cat => cat.id === category).title

    const newRoute =
      category === currentCategory ? '/' : `/category/${categoryName}`
    dispatch(push(newRoute))
    if (currentCategory !== category) {
      dispatch(initSetCategory(category))
    }
  }
}

export const initSetProductSearch = search => ({
  type: SET_PRODUCT_SEARCH,
  payload: search,
})

const initialState = {
  categories: [],
  fetchCategoriesError: null,
  fetchingCategories: false,
  products: [],
  fetchingProductsError: null,
  fetchingProducts: false,
  selectedCategory: null,
  productSearch: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_START: {
      return { ...state, fetchingCategories: true, fetchCategoriesError: null }
    }
    case FETCH_CATEGORIES_SUCCESS: {
      return {
        ...state,
        fetchingCategories: false,
        categories: action.payload,
        fetchCategoriesError: null,
      }
    }
    case FETCH_CATEGORIES_FAILURE: {
      return {
        ...state,
        fetchingCategories: false,
        fetchCategoriesError: action.payload,
      }
    }
    case FETCH_PRODUCTS_START: {
      return { ...state, fetchingProducts: true, fetchProductsError: null }
    }
    case FETCH_PRODUCTS_SUCCESS: {
      return {
        ...state,
        fetchingProducts: false,
        products: action.payload,
        fetchProductsError: null,
      }
    }
    case FETCH_PRODUCTS_FAILURE: {
      return {
        ...state,
        fetchingProducts: false,
        fetchCategoriesError: action.payload,
      }
    }
    case SELECT_CATEGORY: {
      return {
        ...state,
        selectedCategory: action.payload,
      }
    }
    case SET_PRODUCT_SEARCH: {
      return { ...state, productSearch: action.payload }
    }
    default:
      return state
  }
}

// Selectors

export const getCategories = state => state.home.categories
export const getProducts = state => state.home.products
export const getSelectedCategory = state => state.home.selectedCategory
export const getProductSearch = state => state.home.productSearch
export const getProductsForSelectedCategory = createSelector(
  [getProducts, getSelectedCategory],
  (products, selectedCategory) => {
    if (selectedCategory === null) return []
    const selectedProducts = products.filter(product =>
      product.categories.some(category => category.id === selectedCategory)
    )
    return selectedProducts
  }
)
export const getProductsForSelectedCategorySearch = createSelector(
  [getProductsForSelectedCategory, getProductSearch],
  (selectedProducts, search) => {
    return selectedProducts.filter(
      product =>
        product.title.toLowerCase().includes(search.toLowerCase()) ||
        product.description.toLowerCase().includes(search.toLowerCase())
    )
  }
)
